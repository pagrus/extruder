#!/usr/bin/env python3

import requests
from bs4 import BeautifulSoup

# config is stored somewhere, has a list of dicts? or similar
dict = {'slug': 'sfbjv', 'name': 'San Francisco Bay Joint Venture Jobs', 'url': 'https://www.sfbayjv.org/jobs.php'}

cache_dir = '/home/pagrus/gitlab/extruder/cached/'
filename = cache_dir + dict['slug'] + '.txt'

try:
    resp = requests.get(dict['url'])        
    if resp.status_code == 200:
        with open(filename, 'w') as fh:
            fh.write(resp.text)
        print("got html ok")
    else:
        nodice = "about page retrieval failed, response code {}".format(resp.status_code)
        with open(filename, 'w') as fh:
            fh.write(nodice)
            
except requests.exceptions.ConnectionError:
    nodice = "Connection refused for {}". format(dict['name'])
    with open(filename, 'w') as fh:
        fh.write(nodice)
        
with open(filename,'r') as f:
    html = f.read()
soup = BeautifulSoup(html,"html.parser")

table_rows = soup.find_all("tr", class_=["even", "odd"])

item_txt = '''    <item>
      <title>{}</title>
      <description>jobid: {} - title: {} - company: {}</description>
      <pubDate>{}</pubDate>
      <guid>{}</guid>
    </item>'''
    
# url looks like this 
# https://www.sfbayjv.org/job-details.php?index_jobs=1349
basepath = 'https://www.sfbayjv.org/'

items = list()

for tr in table_rows:
    jdict = {}
    jdict['url'] = tr('td')[0]('a')[0]['href']
    jdict['absurl'] = basepath + jdict['url']
    jdict['jobid'] = jdict['url'][27:]
    jdict['posted'] = tr('td')[0]('a')[0].get_text()
    jdict['title'] = tr('td')[1]('a')[0].get_text()
    jdict['company'] = tr('td')[2]('a')[0].get_text()
    jdict['deadline'] = tr('td')[3]('a')[0].get_text()
    
    jdict['enc_title'] = jdict['title'].replace('&', '&amp;')
    jdict['enc_company'] = jdict['company'].replace('&', '&amp;')
    
    thisitem = item_txt.format(jdict['enc_title'], jdict['jobid'], jdict['enc_title'], jdict['enc_company'], jdict['posted'], jdict['absurl'], )
    
    items.append(thisitem)
    
item_txt = "\n".join(items)

rss_head = '''<?xml version="1.0"?>
<rss version="2.0" xmlns:jobstuff="http://pagr.us/">
  <channel>
    <title>San Francisco Bay Joint Venture - Job Board</title>
    <link>http://www.sfbayjv.org</link>
    <description>Job postings from the San Francisco Bay Joint Venture website</description>
'''
rss_foot = '''
  </channel>
</rss>
'''

rss = rss_head + item_txt + rss_foot

print("writing rss file...")
rss_file = "/home/pagrus/gitlab/extruder/sfbjv.rss"
with open(rss_file, 'w') as rf:
    rf.write(rss)